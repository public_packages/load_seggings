======================================
汎用 Python ファイル読み込みパッケージ
======================================

概要
----

Python の文法に沿って書かれたファイルを、拡張子に拘りなくモジュールとし
て読み込みます。
読み込んだモジュールはひとつのオブジェクトとして呼び出し元に返されるの
で、これを変数に代入するなどして使います。

このパッケージが読み込んだモジュールは、システム (sys.modules) には登録
されず、このモジュール自身によって管理されます。
同じファイルを重複して読み込んだ場合、最初に読み込んだ内容をそのまま返し
ます。
このため、あるファイルを読み込んだ後に外部でこれを編集して、編集の後にこ
れを読み込んだとしても編集内容は反映されずに最初に読み込んだ状態のモジュー
ルのまで返されます。

インストール
------------

setuptools の pip コマンドで BitBusket のリポジトリを指定した以下のよう
なインストールを想定しています。

.. code-block::

    pip3 install git+https://bitbucket.org/public_packages/load_seggings.git


使用例
------

.. code-block:: python

    > cat ./SETTING.CFG
    #-*- coding: utf-8 -*-
    url = 'https://ryuu@bitbucket.org/public_packages/load_seggings.git'
    description = 'Loader for setting file by python syntax.'
    days_of_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

.. code-block:: python

    >>> from load_settings import Settings
    >>> my_module = Setting('./SETTING>CFG')
    >>> my_module.url
    'https://ryuu@bitbucket.org/public_packages/load_seggings.git'
    >>> for d in my_module.days_of_month[5:7]:
    >>>     print(d)
    >>> 
    31
    30
    31
    >>> 
