#!/usr/bin/python3
#-*- coding: utf-8 -*-
from __future__ import absolute_import, division, generator_stop, print_function, unicode_literals
try:
    from future_builtins import *
except:
    pass
try:
    import importlib.util
except ImportError:
    import importlib2.util
from pathlib import Path
from inspect import currentframe

IMP_SUFFIX = '*.py'

class _Singleton(type):
    _instances = {}
    
    def __call__(cls, *args, **kwargs):
        ''' インスタンスが関数として「コールされた」ときに呼び出される。
            【戻り値】
                Singleton インスタンス
        '''
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class _Settings(metaclass=_Singleton):
    _settings = {}
    _nametable = {}
    
    @classmethod
    def load_setting(cls, path:str, name:str) -> None:
        fullpath = Path(path).resolve()
        strpath = str(fullpath)
        
        module = None
        if strpath in cls._settings:
            module = cls._settings[strpath]
        if module is None:
            if fullpath.match('*.' + IMP_SUFFIX):
                spec = importlib.util.spec_from_file_location(name, strpath)
            else:
                spec = importlib.util.spec_from_file_location(name, strpath + IMP_SUFFIX)
                if spec is not None:
                    spec.origin = spec.loader.path = strpath
            if spec is not None:
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)
                cls._settings[strpath] = module
        if module:
            cls._nametable[name] = strpath
        elif name in cls._nametable:
            del cls._nametable[name]
        for strpath in cls._settings.keys():
            if strpath not in cls._nametable.values():
                del cls._settings[strpath]
        return module

    @classmethod
    def __getattr__(cls, name:str):
        if name in cls._nametable:
            if cls._nametable[name] in cls._settings:
                return cls._settings[self._nametable[name]]
            del cls._nametable[name]
        raise AttributeError("'module {cls}' has no attribute '{name}'".format(cls=cls.__name__, name=name))

class Setting:
    _setting = None
    
    def __init__(self, path:str, name:str=None) -> None:
        if name is None:
            name = Path(path).name
            idx = name.rfind('.')
            if idx >= 0 and name[:idx] != '.' * idx:
                name = name[:idx]
                idx = name.rfind('.')
            if idx >= 0:
                name = name[idx + 1:]
        self._setting = _Settings.load_setting(path, name)
    
    def __getattr__(self, name:str):
        try:
            return getattr(self._setting, name)
        except AttributeError:
            objs = {id(v):k for k,v in currentframe().f_back.f_locals.items()}
            raise AttributeError("''{inst}' (its is '{module}') has no attribute '{name}'".format(inst=objs.get(id(self)), module=self._setting.__name__, name=name))
